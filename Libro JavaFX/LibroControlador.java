import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.control.Button;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;


public class LibroControlador {

    @FXML
    private TextField txtNumCopias;

    @FXML
    private Label panel1;

    @FXML
    private Label lblTitulo;

    @FXML
    private GridPane panelDatos;

    @FXML
    private Button cmdDevolucion;

    @FXML
    private TextField txtPrecio;

    @FXML
    private Label lblNumCopias;

    @FXML
    private Label lblNumCopiasDisp;

    @FXML
    private TextField txtAutores;

    @FXML
    private Label lblAutores;

    @FXML
    private Label lblAnoEdicion;

    @FXML
    private Label lblEditorial;

    @FXML
    private Label lblNumCopiasPrest;

    @FXML
    private Button cmdPrestamo;

    @FXML
    private TextField txtAnoEdicion;

    @FXML
    private TextField txtEditorial;

    @FXML
    private Label txtNumCopiasPrest;

    @FXML
    private Button cmdActualizar;

    @FXML
    private Label txtNumCopiasDisp;

    @FXML
    private Label lblPrecio;

    @FXML
    private Button cmdGuardar;

    @FXML
    private TextField txtNumPaginas;

    @FXML
    private GridPane panelBotones;

    @FXML
    private Label lblNumPaginas;

    @FXML
    private TextField txtTitulo;
    
    private LibroModelo libro;
    
    public LibroControlador(){
    libro = new LibroModelo();
    }
    
    @FXML
    void guardarCambios() {
    String titulo = txtTitulo.getText();
    String autores = txtAutores.getText();
    String editorial = txtEditorial.getText();
    int anoEdicion = Integer.parseInt(txtAnoEdicion.getText());
    int precio = Integer.parseInt(txtPrecio.getText());
    int numeroDePaginas = Integer.parseInt(txtNumPaginas.getText());
    int numeroDeCopias = Integer.parseInt(txtNumCopias.getText());
    
    if(numeroDeCopias<libro.getNumeroDeCopiasPrestadas()){
    txtNumCopias.setText(""+libro.getNumeroDeCopias());
    Alert alert = new Alert(Alert.AlertType.ERROR, "Cantidad de Copias inferior a la cantidad de Prestamos");
    alert.showAndWait();
    }else{
        
    libro.setTitulo(titulo);
    libro.setAutores(autores);
    libro.setEditorial(editorial);
    libro.setAnoEdicion(anoEdicion);
    libro.setPrecio(precio);
    libro.setNumeroDePaginas(numeroDePaginas);
    libro.setNumeroDeCopias(numeroDeCopias);
    txtNumCopiasDisp.setText(Integer.toString(libro.getNumeroDeCopiasDisponibles()));
    
    Alert alert = new Alert(Alert.AlertType.INFORMATION, "Cambios guardados correctamente"); 
    alert.showAndWait();
    }
    }

    @FXML
    void actualizarPantalla() {
    txtTitulo.setText(libro.getTitulo());
    txtAutores.setText(libro.getAutores());
    txtEditorial.setText(libro.getEditorial());
    txtAnoEdicion.setText(Integer.toString(libro.getAnoEdicion()));
    txtPrecio.setText(Integer.toString(libro.getPrecio()));
    txtNumPaginas.setText(Integer.toString(libro.getNumeroDePaginas()));
    txtNumCopias.setText(Integer.toString(libro.getNumeroDeCopias()));
    }

    @FXML
    void registrarPrestamo() {
    libro.prestar();    
    txtNumCopiasPrest.setText(Integer.toString(libro.getNumeroDeCopiasPrestadas()));
    txtNumCopiasDisp.setText(Integer.toString(libro.getNumeroDeCopiasDisponibles()));
    }

    @FXML
    void registrarDevolucion() {
    libro.devolver();
    txtNumCopiasPrest.setText(Integer.toString(libro.getNumeroDeCopiasPrestadas()));
    txtNumCopiasDisp.setText(Integer.toString(libro.getNumeroDeCopiasDisponibles()));
    }

}

