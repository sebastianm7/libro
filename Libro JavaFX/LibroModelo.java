
/**
 * Write a description of class LibroModelo here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class LibroModelo
{
    
    /**
     * Título del libro
     */
    //COMPLETE
    protected String titulo;

    /**
     * Nombre de la Empresa editorial
     */
    //COMPLETE
    protected String editorial;

    /**
     * Nombre de los autores
     */
    //COMPLETE
    protected String autores;

    /**
     * Año en que fué impreso el libro
     */
    //COMPLETE
    protected int anoEdicion;

    /**
     * Cantidad de hojas-páginas del libro
     */
    //COMPLETE
    protected int numeroDePaginas;

    /**
     * Valor en que se compró el libro
     */
    //COMPLETE
    protected int precio;

    /**
     * Cantidad de ejemplares-copias del libro
     */
    //COMPLETE
    protected int numeroDeCopias;

    /**
     * Cantidad de ejemplares-copias prestados
     */
    //COMPLETE
    protected int numeroDeCopiasPrestadas;

    /**
     * Método Constructor que inicializa con valores por defecto
     */
    public LibroModelo() {
        titulo = "Libro Texto de Programación Orientada a Objetos";
        autores = "Milton Jesús Vera Contreras";
        editorial = "UFPS";
        anoEdicion = 2016;
        precio = 0;
        numeroDePaginas = 100;
        numeroDeCopias = 5;
    }//Fin constructor default

    /**
     * *
     * Método Construye que inicializa las propiedades con los parámetros
     * recibidos
     *
     * @param titulo valor inicial de la propiedad titulo
     * @param editorial valor inicial de la propiedad editorial
     * @param autores valor inicial de la propiedad autores
     * @param añoEdicion valor inicial de la propiedad añoEdicion
     * @param numeroDePaginas valor inicial de la propiedad numeroDePaginas
     * @param precio valor inicial de la propiedad precio
     * @param numeroDeCopias valor inicial de la propiedad numeroDeCopias
     */
    public LibroModelo(int anoEdicion, int numeroDePaginas, int precio, String titulo, String editorial, String autores, int numeroDeCopias) {
        //COMPLETE
        this.anoEdicion = anoEdicion;
        this.numeroDePaginas = numeroDePaginas;
        this.precio = precio;
        this.titulo = titulo;
        this.editorial = editorial;
        this.autores = autores;
        this.numeroDeCopias = numeroDeCopias;
    }//Fin constructor con parámetros

    /**
     * Método de acceso a la propiedad titulo
     *
     * @return el valor de titulo para éste objeto Libro
     */
    public String getTitulo() {
        //COMPLETE
        return this.titulo;
    }//fin getTitulo

    /**
     * *
     * Método para modificación de la propiedad titulo
     *
     * @param titulo el nuevo valor de la propiedad titulo
     */
    public void setTitulo(String titulo) {
        //COMPLETE
        this.titulo = titulo;
    }//fin setTitulo

    /**
     * Método de acceso a la propiedad editorial
     *
     * @return el valor de editorial para éste objeto Libro
     */
    public String getEditorial() {
        //COMPLETE
        return this.editorial;
    }//fin getEditorial

    /**
     * *
     * Método para modificación de la propiedad editorial
     *
     * @param editorial el nuevo valor de la propiedad editorial
     */
    public void setEditorial(String editorial) {
        //COMPLETE
        this.editorial = editorial;
    }//fin setEditorial

    /**
     * Método de acceso a la propiedad autores
     *
     * @return el valor de autores para éste objeto Libro
     */
    public String getAutores() {
        //COMPLETE
        return this.autores;
    }//fin getAutores

    /**
     * *
     * Método para modificación de la propiedad autores
     *
     * @param autores el nuevo valor de la propiedad autores
     */
    public void setAutores(String autores) {
        //COMPLETE
        this.autores = autores;
    }//fin setAutores

    /**
     * Método de acceso a la propiedad añoEdicion
     *
     * @return el valor de añoEdicion para éste objeto Libro
     */
    public int getAnoEdicion() {
        //COMPLETE
        return this.anoEdicion;
    }//fin getAñoEdicion

    /**
     * *
     * Método para modificación de la propiedad añoEdicion
     *
     * @param añoEdicion el nuevo valor de la propiedad añoEdicion
     */
    public void setAnoEdicion(int anoEdicion) {
        //COMPLETE
        this.anoEdicion = anoEdicion;
    }//fin setAñoEdicion

    /**
     * Método de acceso a la propiedad numeroDePaginas
     *
     * @return el valor de numeroDePaginas para éste objeto Libro
     */
    public int getNumeroDePaginas() {
        //COMPLETE
        return this.numeroDePaginas;
    }//fin getNumeroDePaginas

    /**
     * *
     * Método para modificación de la propiedad numeroDePaginas
     *
     * @param numeroDePaginas el nuevo valor de la propiedad numeroDePaginas
     */
    public void setNumeroDePaginas(int numeroDePaginas) {
        //COMPLETE
        this.numeroDePaginas = numeroDePaginas;
    }//fin setNumeroDePaginas

    /**
     * Método de acceso a la propiedad precio
     *
     * @return el valor de precio para éste objeto Libro
     */
    public int getPrecio() {
        //COMPLETE
        return this.precio;
    }//fin getPrecio

    /**
     * *
     * Método para modificación de la propiedad precio
     *
     * @param precio el nuevo valor de la propiedad precio
     */
    public void setPrecio(int precio) {
        //COMPLETE
        this.precio = precio;
    }//fin setPrecio

    /**
     * Método de acceso a la propiedad numeroDeCopias
     *
     * @return el valor de numeroDeCopias para éste objeto Libro
     */
    public int getNumeroDeCopias() {
        //COMPLETE
        return this.numeroDeCopias;
    }//fin getNumeroDeCopias

    /**
     * Método de acceso a la propiedad numeroDeCopiasPrestadas
     *
     * @return el valor de numeroDeCopiasPrestadas para éste objeto Libro
     */
    public int getNumeroDeCopiasPrestadas() {
        //COMPLETE
        return this.numeroDeCopiasPrestadas;
    }//fin getNumeroDeCopiasPestadas

    /*--- Desde aquí comienzan los métodos para satisfacer los Requerimientos---*/
 /*
     * No hay método SET para numeroDeCopiasPrestadas pues ésta propiedad
     * se modifica al prestar (Req2) y devolver (Req3).
     * No hay método SET para numeroDeCopias, en su lugar el método cambiarNumeroDeCopias (Req4)
     */
    /**
     * Determina la cantidad de copias disponibles para prestar (Req1)
     *
     * @return la diferencia entre la cantidad de copias y la cantidad de
     * prestamos
     */
    public int getNumeroDeCopiasDisponibles() {
        //COMPLETE
        return this.numeroDeCopias - this.numeroDeCopiasPrestadas;
    }//fin getNumeroDeCopiasDisponibles

    /**
     * *
     * Registra el préstamo de una de las copias de este libro (Req2)
     *
     * @return true si pudo registrar el prestamo, false si no.
     */
    public boolean prestar() {
        //COMPLETE
        if (this.numeroDeCopias > 0 && this.numeroDeCopiasPrestadas < this.numeroDeCopias) {
            this.numeroDeCopiasPrestadas += 1;
            return true;
        }
        return false;

    } //fin método prestar

    /**
     * *
     * Registra la devolución de una de las copias de este libro (Req3)
     *
     * @return true si pudo registrar la devolución, false si no.
     */
    public boolean devolver() {
        //COMPLETE
        if (this.numeroDeCopiasPrestadas > 0) {
            this.numeroDeCopiasPrestadas -= 1;
            return true;
        }
        return false;
    }//fin método devolver

    /**
     * *
     * Método para modificación de la propiedad numeroDeCopias. (Req4) Controla
     * que no se reduzca numeroDeCopias a un valor inconsistente: numeroDeCopias
     * no puede ser menor que numeroDeCopiasPrestadas
     *
     * @param numeroDeCopias el nuevo valor de la propiedad numeroDeCopias
     * @return true si pudo cambiar el valor, false si no se pudo.
     */
    public boolean cambiarNumeroDeCopias(int numeroDeCopias) {
        //COMPLETE
        if(numeroDeCopias < this.numeroDeCopiasPrestadas){
            return false;
        }
        this.numeroDeCopias = numeroDeCopias;
        return true;
    }//fin setNumeroDeCopias
    
    public void setNumeroDeCopias(int numeroDeCopias) {
        this.numeroDeCopias = numeroDeCopias;
    }
    
    /**
     * *
     * Regresa una cadena String con los datos del Libro
     *
     * @override java.lang.Object.toString
     */
    public String toString() {
        String str = "\n**********Libro**********\n";
        str = str + "Titulo: " + this.getTitulo() + "\n";
        str = str + "Autores: " + this.getAutores() + "\n";
        str = str + "Editorial: " + this.getEditorial() + "\n";
        str = str + "Año: " + this.getAnoEdicion() + "\n";
        str = str + "Paginas: " + this.getNumeroDePaginas() + "\n";
        str = str + "Precio: " + this.getPrecio() + "\n";
        str = str + "Copias: " + this.getNumeroDeCopias() + "\n";
        str = str + "Copias Disponibles: " + this.getNumeroDeCopiasDisponibles() + "\n";
        str = str + "Copias Prestadas: " + this.getNumeroDeCopiasPrestadas() + "\n";
        return str;
    }
}
